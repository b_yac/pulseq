# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 16:15:25 2020

@author: Yacine Bencheikh
"""
import h5py
import numpy as np
from scipy.fft import fftn, fftshift
import matplotlib.pyplot as plt

#### Lecture du fichier ####
filedata = h5py.File('meas_MID15_2D_1slice_GRE_cartes_FOV_trans_FID15130.h5', 'r')
dataset = filedata['dataset/data']

#### Réagencement du kspace & reconstruction ####
Nx = 256
Ny = 256
Nz = 7
kspace = np.zeros((Nx,Ny,Nz),dtype=complex)

for i in range(len(dataset)):
    if i < 7 : kspace[0,:,i] = dataset[i]['data'][::2]+ 1j*dataset[i]['data'][1::2]
    line, slc = np.divmod(i,Nz)
    kspace[line,:,slc] = dataset[i]['data'][::2]+ 1j*dataset[i]['data'][1::2]    
#shiftmat = np.dstack([np.tile([[1,-1],[-1,1]],(128,128))]*7)
#kspace = kspace*shiftmat
#img = fftn(kspace,axes=(0,1))*shiftmat
img = fftshift(fftn(kspace,axes=(0,1)),axes=(0,1))

## Affichage des images
plt.figure(1)
plt.imshow(np.abs(img[:,:,0]), cmap= 'gray')
plt.title('Slice 1')

plt.figure(2)
plt.imshow(np.abs(img[:,:,1]), cmap= 'gray')
plt.title('Slice 2')

plt.figure(3)
plt.imshow(np.abs(img[:,:,2]), cmap= 'gray')
plt.title('Slice 3')
    
plt.figure(4)
plt.imshow(np.abs(img[:,:,3]), cmap= 'gray')
plt.title('Slice 4')

plt.figure(5)
plt.imshow(np.abs(img[:,:,4]), cmap= 'gray')
plt.title('Slice 5')

plt.figure(6)
plt.imshow(np.abs(img[:,:,5]), cmap= 'gray')
plt.title('Slice 6')

plt.figure(7)
plt.imshow(np.abs(img[:,:,6]), cmap= 'gray')
plt.title('Slice 7')