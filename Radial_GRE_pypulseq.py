import math
import numpy as np
from types import SimpleNamespace

from pypulseq.Sequence.sequence import Sequence
from pypulseq.calc_duration import calc_duration
from pypulseq.make_adc import make_adc
from pypulseq.make_delay import make_delay
from pypulseq.make_sinc_pulse import make_sinc_pulse
from pypulseq.make_trap_pulse import make_trapezoid
from pypulseq.opts import Opts

seq = Sequence()
fov = 250e-3
Nx = 256
Nslice = 7
alpha = 10*(math.pi/180)
slice_thickness = 3e-3
TE = 8e-3
TR = 100e-3
Ns = 128
Ndummy = 20
deltaphi = math.pi/Ns
rf_dur = 4e-3
timebw_product = 4
freqOffSet = [ x*(timebw_product/rf_dur) for x in range(0,2*Nslice-1,2)]

sys = Opts(max_grad=28, grad_unit='mT/m', max_slew=80, slew_unit='T/m/s', rf_ringdown_time=20e-6, rf_dead_time=100e-6,
           adc_dead_time=10e-6)

rf, gz, gzr = make_sinc_pulse(flip_angle=alpha, duration=rf_dur, slice_thickness=slice_thickness,apodization=0.5,
                              time_bw_product=timebw_product,  system=sys)

delta_k = 1 / fov
gx = make_trapezoid(channel='x', flat_area=(Nx * delta_k), flat_time=6.4e-3, system=sys)
adc = make_adc(num_samples=Nx, duration=gx.flat_time, delay=gx.rise_time, system=sys)
gx_pre = make_trapezoid(channel='x', area=-gx.area / 2, duration=2e-3, system=sys)
gz_reph = make_trapezoid(channel='z', area=-gz.area / 2, duration=2e-3, system=sys)

delay_TE = np.ceil((TE - calc_duration(gx_pre) - gz.fall_time - gz.flat_time / 2 - calc_duration(
    gx) / 2) / seq.grad_raster_time) * seq.grad_raster_time
delay_TR = np.ceil((TR - Nslice*(calc_duration(gx_pre) + calc_duration(gz) + calc_duration(
    gx) + delay_TE)) / seq.grad_raster_time) * seq.grad_raster_time


for i in range(-Ndummy,Ns):
    for k in range(len(freqOffSet)):
        rf.freq_offset = freqOffSet[k]
        seq.add_block(rf, gz)
        phi = deltaphi*(i-1)
        gpc = SimpleNamespace(**gx_pre.__dict__)
        gps = SimpleNamespace(**gx_pre.__dict__)
        gps.channel = 'y'
        gpc.amplitude = gx_pre.amplitude*math.cos(phi)
        gps.amplitude = gx_pre.amplitude*math.sin(phi)
        grc = SimpleNamespace(**gx.__dict__)
        grs = SimpleNamespace(**gx.__dict__)
        grs.channel = 'y'
        grc.amplitude = gx.amplitude*math.cos(phi)
        grs.amplitude = gx.amplitude*math.sin(phi)
        seq.add_block(gpc, gps, gz_reph)
        seq.add_block(make_delay(delay_TE))
        if i>0 : 
            seq.add_block(grc, grs, adc)
        else:
            seq.add_block(grc,grs)
    seq.add_block(make_delay(delay_TR))

seq.plot(time_range=(20*TR, 23*TR))
seq.set_definition('FOV', np.array([fov, fov, slice_thickness]) * 1e3)
seq.write('Radial_GRE_pypulseq.seq')